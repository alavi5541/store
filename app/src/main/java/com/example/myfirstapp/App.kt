package com.example.myfirstapp

import android.app.Application
import com.example.myfirstapp.repo.repository.ProductRepository
import com.example.myfirstapp.repo.repository.ProductRepositoryImpl
import com.example.myfirstapp.repo.source.ProductLocalDataSource
import com.example.myfirstapp.repo.source.ProductRemoteDataSource
import com.example.myfirstapp.services.createApiServiceInstance
import com.example.myfirstapp.viewmodel.ProductViewModel

import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        val myModules= module{
            single{ createApiServiceInstance() }
            factory<ProductRepository> { ProductRepositoryImpl(ProductRemoteDataSource(get()),ProductLocalDataSource()) }
            viewModel{ProductViewModel(get())}
        }
        startKoin{
            androidContext(this@App)
            modules(myModules)
        }

    }

}