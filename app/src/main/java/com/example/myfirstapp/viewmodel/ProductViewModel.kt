package com.example.myfirstapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myfirstapp.common.BaseViewModel
import com.example.myfirstapp.repo.Resource
import com.example.myfirstapp.repo.repository.ProductRepository
import com.example.store.model.Product
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ProductViewModel(private val productRepository: ProductRepository) : BaseViewModel() {

    fun getProducts():LiveData<Resource<List<Product>?>>{
        return getResourceLiveData(productRepository.getProducts(true))
    }
}