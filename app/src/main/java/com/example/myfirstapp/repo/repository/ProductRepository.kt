package com.example.myfirstapp.repo.repository

import com.example.store.model.Product
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface ProductRepository {
    fun getProducts(isRemote:Boolean): Observable<List<Product>>
    fun getFavoriteProduct(isRemote:Boolean): Observable<List<Product>>
    fun addToFavorites(isRemote:Boolean):Observable<Any>
    fun deleteFromFavorites(isRemote:Boolean):Observable<Any>
}