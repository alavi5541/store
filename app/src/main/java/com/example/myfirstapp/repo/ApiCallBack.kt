package com.example.myfirstapp.repo

import retrofit2.HttpException

interface ApiCallBack<T> {
    fun onSuccessFullResponse(response: T)
    fun onError(error: String?)
}