package com.example.myfirstapp.repo.source

import com.example.store.model.Product
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class ProductLocalDataSource:ProductDataSource {
    override fun getProduct(): Observable<List<Product>> {
        TODO("Not yet implemented")
    }

    override fun getFavoriteProduct(isRemote: Boolean): Observable<List<Product>> {
        TODO("Not yet implemented")
    }

    override fun addToFavorites(isRemote: Boolean): Observable<Any> {
        TODO("Not yet implemented")
    }

    override fun deleteFromFavorites(isRemote: Boolean): Observable<Any> {
        TODO("Not yet implemented")
    }

}