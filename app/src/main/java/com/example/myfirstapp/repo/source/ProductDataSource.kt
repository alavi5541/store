package com.example.myfirstapp.repo.source

import com.example.store.model.Product
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface ProductDataSource {
    fun getProduct(): Observable<List<Product>>
    fun getFavoriteProduct(isRemote:Boolean): Observable<List<Product>>
    fun addToFavorites(isRemote:Boolean): Observable<Any>
    fun deleteFromFavorites(isRemote:Boolean): Observable<Any>
}