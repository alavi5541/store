package com.example.myfirstapp.repo.repository

import com.example.myfirstapp.repo.repository.ProductRepository
import com.example.myfirstapp.repo.source.ProductDataSource
import com.example.store.model.Product
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class ProductRepositoryImpl (
    private val productRemoteDataSource: ProductDataSource
    , private val productLocalDataSource: ProductDataSource): ProductRepository {

    override fun getProducts(isRemote: Boolean): Observable<List<Product>> = if (isRemote)productRemoteDataSource.getProduct()
       else productLocalDataSource.getProduct()

    override fun getFavoriteProduct(isRemote: Boolean): Observable<List<Product>> {
        TODO("Not yet implemented")
    }

    override fun addToFavorites(isRemote: Boolean): Observable<Any> {
        TODO("Not yet implemented")
    }

    override fun deleteFromFavorites(isRemote: Boolean): Observable<Any> {
        TODO("Not yet implemented")
    }


}