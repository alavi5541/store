package com.example.myfirstapp.repo

import com.google.gson.Gson
import android.os.Parcelable
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import java.util.ArrayList


/**
 * A generic class that holds a value with its loading status.
 *
 * @param <T>
</T> */
open class Resource<T>(
     val status: Status,
     val data: T?,
     val errorMessage: String?
) {
    override fun hashCode(): Int {
        return HashCodeBuilder().append(status).append(errorMessage).append(data).toHashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other == null || javaClass != other.javaClass) {
            return false
        }
        val resource = other as Resource<*>
        return EqualsBuilder()
            .append(status, resource.status)
            .append(data, resource.data)
            .append(errorMessage, resource.errorMessage)
            .isEquals
    }

    override fun toString(): String {
        val gson = Gson()
        return gson.toJson(this)
    }

    val isSuccessful: Boolean
        get() = status == Status.SUCCESS

    companion object {
        fun <T> success(data: T?): Resource<T?> {
            return Resource(Status.SUCCESS, data,  null)
        }

        fun <T : Parcelable?> success(data: ArrayList<T>?): Resource<ArrayList<T>?> {
            return Resource(Status.SUCCESS, data,  null)
        }

        fun <T> error( msg: String?): Resource<T?> {
            return Resource(Status.ERROR, null, msg)
        }

        @JvmStatic
        fun <T> error(data: T?, msg: String?): Resource<T?> {
            return Resource(Status.ERROR, data,  msg + "")
        }

        @JvmStatic
        fun <T> loading(data: T?): Resource<T?> {
            return Resource(Status.LOADING, data,  null)
        }
    }
}