package com.example.myfirstapp.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.myfirstapp.R
import com.example.myfirstapp.common.BaseFragment
import com.example.myfirstapp.repo.Status
import com.example.myfirstapp.viewmodel.ProductViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class HomeFragment : BaseFragment() {
    private var param1: String? = null
    private var param2: String? = null
    private val productViewModel: ProductViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        productViewModel.getProducts().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Timber.d("getProducts %s",Status.LOADING)
                }
                Status.SUCCESS ->{
                    Log.d("getProducts",it.data.toString())
                    Toast.makeText(context,"sucsses",Toast.LENGTH_LONG).show()
                    Timber.d("getProducts %s %s",Status.LOADING,it.data.toString())
                }
                Status.ERROR->{
                    Timber.d("getProducts %s",Status.ERROR)

                }

            }
        })
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}