package com.example.myfirstapp.common

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myfirstapp.repo.ApiCallBack
import com.example.myfirstapp.repo.Resource
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import org.apache.commons.lang3.tuple.Pair
import timber.log.Timber
import java.lang.Exception
import java.util.concurrent.TimeUnit
import kotlin.math.pow

abstract class BaseFragment:Fragment(),StoreView{
    override fun setProgressIndicator(mustShow: Boolean) {
        TODO("Not yet implemented")
    }
}


abstract class BaseActivity:AppCompatActivity(),StoreView{
    override fun setProgressIndicator(mustShow: Boolean) {
        TODO("Not yet implemented")
    }
}


interface StoreView{
    fun setProgressIndicator(mustShow:Boolean)
}

abstract class BaseViewModel:ViewModel(){
    open val compositeDisposable=CompositeDisposable()
    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    open fun <T> getResourceLiveData(observable: Observable<T>?): LiveData<Resource<T?>> {
        val result: MutableLiveData<Resource<T?>> = MutableLiveData<Resource<T?>>()
        run<T>(observable, object : ApiCallBack<T> {
            override fun onSuccessFullResponse(response: T) {
                result.postValue(Resource.success(response))
            }

            override fun onError(error: String?) {
                Timber.e("Api Error: \n %s", error)
                result.postValue(Resource.error(null, error))
            }
        })
        return result
    }


    @SuppressLint("CheckResult")
    open fun <T> run(input: Observable<T>?, callback: ApiCallBack<T>) {
        input?.let{
            compositeDisposable.add(input
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retryWhen { error: Observable<Throwable?> ->
                    error
                        .zipWith<Int, Pair<Throwable, Int>>(
                            Observable.range(1, 3),
                            { left: Throwable?, right: Int? ->
                                Pair.of(
                                    left,
                                    right
                                )
                            })
                        .flatMap { pair: Pair<Throwable, Int> ->
                            if (pair.right ==
                                3
                            ) {
                                Observable.error<Any>(pair.left)
                                throw Exception(pair.left)
                            }
                            Observable.timer(
                                3.0.pow(pair.right.toDouble()).toLong(),
                                TimeUnit.SECONDS
                            )
                        }
                }
                .subscribe(callback::onSuccessFullResponse) { throwable: Throwable ->
                    //Case of Error
                    val cause = throwable.cause
                    callback.onError(throwable.message)
                }
            )
        }

    }

}